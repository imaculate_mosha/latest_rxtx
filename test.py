import serial
import time

port = serial.Serial("/dev/serial/by-id/usb-Digilent_Digilent_USB_Device_210292696944-if01-port0", baudrate=100000, timeout=500.0, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)
#port.write(reversebytes(pad("a"))) 

chunks = 2
time.sleep(0.01)

print "Sending to accelerator..."
# Send 4 bytes of size
port.write(chr(0))
time.sleep(0.01)

port.write(chr(0))
time.sleep(0.01)

port.write(chr(0))
time.sleep(0.01)

port.write(chr(chunks))
time.sleep(0.01)
# Send chunks
print "Receiving ack for number of chunks"
while(not port.inWaiting()):
  pass
print ord(port.read())
for j in range(chunks):
  for i in range(64):
    #print i
    port.write(chr(i))
  while(not port.inWaiting()):
    pass
  print ord(port.read())
  print "Receiving ack for chunk no: ", i
print "Sending done."

port.flush()


print "Receiving final output from accelerator..."
for i in range(16):
  while(not port.inWaiting()):    
    pass #do nothing
  print format(ord(port.read()), '02x')
print "Receiving done."
