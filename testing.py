import serial
import time



def pad(strn):
	
	bst = ""
	for i in range(len(strn)):
		bst += format(ord(strn[i]) , '08b')
	ln = len(bst)
	bst += '1'
	
	ln2 = ln +1;
	j = ln2/448;
	#append 448 - ln2 0zeros to strn
	for i in range((j+1)* 448 - ln2):
		bst += '0'
		
	bst += to64binary(ln)
	#512*n length bitstring
	return bst


def to64binary(ln):
	l =  ln & ((1<<64) -1 )
	l= format(l, '064b')
	
	return l


port = serial.Serial("/dev/serial/by-id/usb-Digilent_Digilent_USB_Device_210292696944-if01-port0", baudrate=100000, timeout=500.0, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)
#port.write(reversebytes(pad("a"))) 

print "Sending to accelerator..."
#small string that can fit to 512 bit chunk 
st = pad("brown dog")
# Send chunk
for i in range(64):
    #print i
	nextbyte =  chr(int( st[i*8:(i+1)*8],2)) 
	port.write(nextbyte)

print "Sending done."
port.flush()



print "Receiving final output from accelerator..."
for i in range(16):
	while(not port.inWaiting()):    
		pass #do nothing
	print format(ord(port.read()), '02x')
print "Receiving done."

 


