`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:33:37 05/18/2016 
// Design Name: 
// Module Name:    RX_Control 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RX_Control(
	input clk,
  input Reset,
	input Rx_In,
	output Tx_Out,
	output[15:0] LEDS
	
);

localparam Idle = 3'd0;
localparam ReceivingSize = 3'd1;
localparam Receiving = 3'd2;
localparam Hashing = 3'd3;
localparam Sending = 3'd4;
localparam AckSize = 3'd5;
localparam AckChunk = 3'd6;

reg [1:0]State ;
reg Ack;
wire Rx_Ready;
reg Previous_Rx_Ready;
wire [7:0]Rx_Data;
reg [2:0] sizeBytesReceived;
reg [2:0] localSizeBytes;
reg [7:0] bytesReceived;
reg [7:0] bytesSent;
reg hasReceived;
reg [7:0] Tx_Data;
reg Send;
wire Busy;
reg Previous_Busy;
reg Ready_To_Send;
reg startHashing;
wire md5done;
reg [31:0]numberOfChunks;
reg [31:0]chunksHashed;
reg [511:0] Chunk;
reg [31:0] a0;
reg [31:0] b0; 
reg [31:0] c0;
reg [31:0] d0; 
wire [31:0] a64; 
wire [31:0] b64; 
wire [31:0] c64; 
wire [31:0] d64;
UARTRx rx(clk,Rx_In, Ack,Rx_Ready, Rx_Data );
UARTTx tx(Tx_Data,Send,clk,Tx_Out, Busy);
Md5Core md5(startHashing, clk,  Reset,Chunk, a0,  b0,c0, d0,a64,b64, c64,d64,md5done);

reg [31:0] CV_a;
reg [31:0] CV_b;
reg [31:0] CV_c;
reg [31:0] CV_d;

always @(posedge clk) begin
  if(Reset) begin
    State <= Idle;
    hasReceived <= 1'b0;
    Previous_Busy <= 1'b0;
    startHashing <= 1'b0;
  end else begin

    case(State)
      Idle: begin
        Ready_To_Send <= 1'b1;
        Tx_Data <= 8'b0;
        a0 <= 32'h67452301;
        b0 <= 32'hefcdab89;
        c0 <= 32'h98badcfe;
        d0<= 32'h10325476;
        if(Rx_Ready && !hasReceived) begin
          hasReceived <= 1'b1;
          State <= ReceivingSize;
          sizeBytesReceived <= 3'b0;
          bytesReceived <= 8'b0;
          bytesSent <= 8'b0;
          Previous_Rx_Ready <= 1'b0;
        end
        Send<= 1'b0;
      end
      
      // Receive 4 bytes indicating the number of chunks to follow
      ReceivingSize: begin
        if(sizeBytesReceived == 3'd4) begin
            chunksHashed <= 32'd0;
            State <= AckSize;
        end
        else if(Rx_Ready && !Previous_Rx_Ready) begin
          Ack <= 1'b1;
          numberOfChunks <= {numberOfChunks[23:0], Rx_Data};
          sizeBytesReceived <= sizeBytesReceived + 3'd1;
          localSizeBytes <= sizeBytesReceived;
        end else if(!Rx_Ready)
          Ack <= 1'b0; //reset
        Previous_Rx_Ready <= Rx_Ready;
      end
      
      AckSize: begin
        if(!Busy && Previous_Busy) begin
          Tx_Data <= 8'b10101010;
          Send <= 1'b1;
        end else if(Busy) begin
          Send <= 1'b0;
          State <= Receiving;
        end
        Previous_Busy <= Busy;
      end
      
      // Receive 64 bytes of Chunk
      Receiving: begin
        if(Rx_Ready) begin
          Ack <= 1'b1;
          Chunk <= {Chunk[503:0], Rx_Data};
          if(bytesReceived == 8'd63) begin
            State <= Hashing;
            startHashing <= 1'b1;
          end else begin
            bytesReceived <= bytesReceived+ 8'b1;
          end
        end else
          Ack <= 1'b0; //reset
      end
      
      Hashing: begin
        if(md5done)begin
          chunksHashed <= chunksHashed + 8'd1;
          startHashing <= 1'b0;
          //take output from md5core
          CV_a <= a64;
          CV_b <= b64;
          CV_c <= c64;
          CV_d <= d64;
          State <= AckChunk;
        end
      end
      
      AckChunk: begin
        // On Busy falling edge...
        if(!Busy && Previous_Busy) begin
          Tx_Data <= 8'b10101010;
          Send <= 1'b1;
        end else if(Busy) begin
          Send <= 1'b0;
           // If all chunks have been hashed, send result:
          if(chunksHashed == numberOfChunks) begin
            State <= Sending;
          // Or else input result of current hash and get new chunk
          end else begin
            a0 <= CV_a;
            b0 <= CV_b;
            c0 <= CV_c;
            d0 <= CV_d;
            State <= Receiving;
          end
        end
        Previous_Busy <= Busy;
      end
      
      Sending:begin
        // On Busy falling edge...
        if(!Busy && Previous_Busy) begin
          Ready_To_Send <= 1'b1;
        end
        else if(Busy)
          Send <= 1'b0;
        else if(Ready_To_Send) begin
          Ready_To_Send <= 1'b0;
          if(bytesSent < 8'd16) begin
            if(bytesSent == 8'd0) 
              Tx_Data <= CV_a[7:0];
            else if(bytesSent == 8'd1) 
              Tx_Data <= CV_a[15:8];
            else if(bytesSent == 8'd2) 
              Tx_Data <= CV_a[23:16]; 
            else if(bytesSent == 8'd3) 
              Tx_Data <= CV_a[31:24];
            else if(bytesSent == 8'd4) 
              Tx_Data <= CV_b[7:0];
            else if(bytesSent == 8'd5) 
              Tx_Data <= CV_b[15:8];
            else if(bytesSent == 8'd6) 
              Tx_Data <= CV_b[23:16];          
            else if(bytesSent == 8'd7) 
              Tx_Data <= CV_b[31:24];
            else if(bytesSent == 8'd8) 
              Tx_Data <= CV_c[7:0];
            else if(bytesSent == 8'd9) 
              Tx_Data <= CV_c[15:8];
            else if(bytesSent == 8'd10) 
              Tx_Data <= CV_c[23:16]; 
            else if(bytesSent == 8'd11) 
              Tx_Data <= CV_c[31:24];
            else if(bytesSent == 8'd12) 
              Tx_Data <= CV_d[7:0];
            else if(bytesSent == 8'd13) 
              Tx_Data <= CV_d[15:8];
            else if(bytesSent == 8'd14) 
              Tx_Data <= CV_d[23:16];          
            else if(bytesSent == 8'd15) 
              Tx_Data <= CV_d[31:24];
            Send <= 1'b1;
          end else if(bytesSent == 8'd16 ) begin
            State <= Idle;
            hasReceived <= 1'b0;
            Send <= 1'b0;
          end
          bytesSent <= bytesSent + 8'b1;
        end
        Previous_Busy <= Busy;
      end
    endcase
  end
end

assign LEDS[15:8] = numberOfChunks[7:0];
assign LEDS[7:5] = State;
assign LEDS[4:3] = {startHashing, md5done};
assign LEDS[2:0] = localSizeBytes[2:0];
//assign LEDS[5] = Send;
//assign LEDS[4] = LocalSent;
//assign LEDS[4:0] = bytesSent[3:0];

endmodule
