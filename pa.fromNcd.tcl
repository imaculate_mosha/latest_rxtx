
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name Rx_Control -dir "/home/bluelabuser/Desktop/latest_rxtx/planAhead_run_1" -part xc7a100tcsg324-3
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "/home/bluelabuser/Desktop/latest_rxtx/RX_Control.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/bluelabuser/Desktop/latest_rxtx} }
set_property target_constrs_file "RX_Control.ucf" [current_fileset -constrset]
add_files [list {RX_Control.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "/home/bluelabuser/Desktop/latest_rxtx/RX_Control.ncd"
if {[catch {read_twx -name results_1 -file "/home/bluelabuser/Desktop/latest_rxtx/RX_Control.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"/home/bluelabuser/Desktop/latest_rxtx/RX_Control.twx\": $eInfo"
}
