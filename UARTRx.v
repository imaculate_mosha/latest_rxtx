`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:24:55 04/20/2016 
// Design Name: 
// Module Name:    UARTRx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module UARTRx(
    input 			clk,	//Clock to the system
	 input 			UARTIn, //Data on UART RxLine
	 input 			byteAcknowledged,//This input from the system indicates the top level module has received the byte
	 output			byteReady,//THis output to the system indicates that a byte is ready for the top level module to receive
	 output[7:0]	receivedByte
	 );

reg [17:0]	Count; //Random Count

//UART Testing

reg [7:0]	UARTByteIn = 8'b00000001;
reg [2:0]	index;
reg			receiving = 0;
reg			byteReadyReg;

always @(posedge clk) begin
	if(Count == 1000) begin
		if(receiving == 0 && UARTIn == 0 & byteReadyReg == 0) begin
			receiving <= 1;
		end
		
		if(receiving == 1)begin
			case (index)
				3'b000: UARTByteIn[0] <= UARTIn;
				3'b001: UARTByteIn[1] <= UARTIn;
				3'b010: UARTByteIn[2] <= UARTIn;
				3'b011: UARTByteIn[3] <= UARTIn;
				3'b100: UARTByteIn[4] <= UARTIn;
				3'b101: UARTByteIn[5] <= UARTIn;
				3'b110: UARTByteIn[6] <= UARTIn;
				3'b111: begin  //The last bit is sent and stop signal flag is set to high
								UARTByteIn[7] <= UARTIn;
								receiving <= 0;
								byteReadyReg <= 1;
						end
			endcase
			index<=index+1;
		end
		
		Count<=0;
	end else begin
		Count <= Count +1'b1;
	end
	
	//Once the byte has been acknoledged from the top level module, the module will be ready to receive data again
	if(byteAcknowledged == 1 && receiving == 0) begin	
		byteReadyReg <= 0;
	end
end
	
assign receivedByte = UARTByteIn;
assign byteReady = byteReadyReg;

endmodule
