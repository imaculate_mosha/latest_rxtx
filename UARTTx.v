module UARTTx(
  input[7:0] Data, // Byte of data to send
  input Send,
  input Clk,
 
  
  output reg Tx,
  output reg Busy
);

// States
localparam Idle =     2'b00;
localparam Sending =  2'b01;
localparam Stopping = 2'b10;
localparam Done =     2'b11;

reg [1:0]State;
initial State <= Idle;

reg [19:0]Count;
reg [2:0]Sent;
reg [7:0]LocalData;

reg LocalSend;

always @(posedge Clk) begin
  LocalSend <= Send;
  
  if(Count != 20'd999) begin
    Count <= Count + 1'b1;
  end  
  else begin
    Count <= 0;
	 
    case(State)
	   Idle: begin
		  if(LocalSend) begin
          LocalData <= Data;
          Sent <= 3'd0;
          // Start bit
          Tx <= 1'b0;
          Busy <= 1'b1;
          State <= Sending;
        end
      end
		
      Sending: begin
        // Shift LS bit onto line
        {LocalData[6:0], Tx} <= LocalData;
        if(Sent == 3'd7) begin
          State <= Stopping;
        end
        Sent <= Sent + 1'b1;
      end
		
      Stopping: begin
        // Stop bit
        Tx <= 1'b1;
        State <= Done;
      end
		
      Done: begin
        Busy <= 1'b0;
        State <= Idle;
      end
      
      default:;
    endcase
  end
end
endmodule
